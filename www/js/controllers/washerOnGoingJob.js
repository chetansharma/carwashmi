app.controller('washerOnGoingJobCtrl', function ($scope, $rootScope, $http, apiUrl, apiPassword, apiUsername, $stateParams, $state, $ionicLoading, store, $timeout, $ionicSideMenuDelegate, $interval, $cordovaPushV5, PaypalService, $cordovaDialogs) {

  $scope.clientRequestId = $stateParams.client_request_id;
  $scope.clientRequest = {};

  $scope.$on('$ionicView.afterEnter', function () {
    $scope.getClientRequest($scope.clientRequestId);
  });

  var ongoingRef = firebase.database().ref('ongoing/' + $scope.clientRequestId)
  var jobStartedRef = firebase.database().ref('jobstarted/' + $scope.clientRequestId)

  ongoingRef.on('value', function (snap) {
    var val = snap.val();
    if (val && val == 1) {
      if (store.get("userType") == "0") {

      } else if (store.get("userType") == "1") {
        $state.go("rating-washer", {
          "requestId": $scope.clientRequest.client_request_id,
          "userId": $scope.clientRequest.user_id,
        });
      }
    }
  })

  $scope.getClientRequest = function (id) {
    $ionicLoading.show();
    var data = {};
    data.username = apiUsername;
    data.pwd = apiPassword;
    data.action = "getclientrequest";
    data.client_request_id = id;
    $.ajax({
      type: 'POST',
      url: apiUrl,
      data: data,
      contentType: "application/x-www-form-urlencoded",
      crossDomain: true
    }).then(function (data) {
      $ionicLoading.hide();
      if (data && data.code == 200) {
        $scope.clientRequest = data.response;
      }
      $scope.$digest();
    }, null);
  }

  $scope.distance = function (lat1, lon1, lat2, lon2) {
    var radlat1 = Math.PI * lat1 / 180
    var radlat2 = Math.PI * lat2 / 180
    var theta = lon1 - lon2
    var radtheta = Math.PI * theta / 180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist)
    dist = dist * 180 / Math.PI
    dist = dist * 60 * 1.1515
    return dist
  };

  $scope.setJobDone = function () {
    var data1 = {};
    data1.username = apiUsername;
    data1.pwd = apiPassword;
    data1.action = "updaterequeststatus";
    data1.client_request_id = $scope.clientRequest.client_request_id;
    data1.user_id = $scope.clientRequest.user_id;
    data1.number = '6';
    $.ajax({
      type: 'POST',
      url: apiUrl,
      data: data1,
      contentType: "application/x-www-form-urlencoded",
      crossDomain: true
    }).then(function (data2) {
      if (data2.success == true && data2.code == 200) {

        var data = {};
        data.username = apiUsername;
        data.pwd = apiPassword;
        data.action = "pushnotifications";
        data.user_id = $scope.clientRequest.user_id;
        data.client_request_id = $scope.clientRequest.client_request_id;
        data.message = 'Job Completed Successfully!';
        $.ajax({
          type: 'POST',
          url: apiUrl,
          data: data,
          contentType: "application/x-www-form-urlencoded",
          crossDomain: true
        }).then(function (data) {
          $ionicLoading.hide();
          jobStartedRef.set(1);
          if (data && data.code == 200) {
            $ionicLoading.show({
                template: 'Job Completed Successfully!',
                duration: 2000
            });
          }
          $scope.$digest();
        }, null);


        if ($scope.clientRequest.default_method == "paypal") {
          ongoingRef.set(1)
          var total = $scope.clientRequest.points;
          PaypalService.initPaymentUI().then(function () {
            PaypalService.makePayment(total, "Total Amount", $http, 'b', $ionicLoading, apiUsername, apiPassword, $scope.clientRequest.user_id, apiUrl, $scope.clientRequest.client_request_id, $state).then(function (response) {
              $cordovaDialogs.alert("get refresh token", "Alert", "Close");
            }, function (error) {
              $cordovaDialogs.alert("Transaction Canceled", "Alert", "Close");
            });
          });

        } else {

          var total = $scope.clientRequest.points;
          var data1 = {};
          data1.username = apiUsername;
          data1.pwd = apiPassword;
          data1.action = "cardpayment";
          data1.client_request_id = $scope.clientRequest.client_request_id;
          data1.user_id = $scope.clientRequest.user_id;
          data1.amount = total;
          $.ajax({
            type: 'POST',
            url: apiUrl,
            data: data1,
            contentType: "application/x-www-form-urlencoded",
            crossDomain: true
          }).then(function (data) {
            $ionicLoading.hide();
            ongoingRef.set(1)
            if (data.success == "success") {
              $cordovaDialogs.alert("Payment Done!", "Success", "Ok");
              var data1 = {};
              data1.username = apiUsername;
              data1.pwd = apiPassword;
              data1.action = "updaterequeststatus";
              data1.client_request_id = $scope.clientRequest.client_request_id;
              data1.user_id = $scope.clientRequest.user_id;
              data1.number = '7';
              $.ajax({
                type: 'POST',
                url: apiUrl,
                data: data1,
                contentType: "application/x-www-form-urlencoded",
                crossDomain: true
              }).then(function (data2) {
                $scope.$digest();
              }, function (error) {
                $rootScope.counter = $rootScope.counter + 1;
                $ionicLoading.hide();
              });


            } else {
              $cordovaDialogs.alert("Payment Failed!", "Error", "Close");
              var data1 = {};
              data1.username = apiUsername;
              data1.pwd = apiPassword;
              data1.action = "userblock";
              data1.user_id = $scope.clientRequest.user_id;

              $.ajax({
                type: 'POST',
                url: apiUrl,
                data: data1,
                contentType: "application/x-www-form-urlencoded",
                crossDomain: true
              }).then(function (data2) {
                $scope.$digest();
              }, function (error) {
                $rootScope.counter = $rootScope.counter + 1;
                $ionicLoading.hide();
              });

            }
          }, function (error) {
            $ionicLoading.hide();
          });
        }


      }
      $scope.$digest();
    }, function (error) {
      $rootScope.counter = $rootScope.counter + 1;
      $ionicLoading.hide();
    });
  }


});
