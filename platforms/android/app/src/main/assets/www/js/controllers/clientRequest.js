app.controller('ClientRequestCtrl', function ($scope, $rootScope, $http, apiUrl, $state, $ionicLoading, store, apiUsername, apiPassword, $interval, $stateParams, $ionicPopup, $cordovaDialogs) {
  var list = [];
  $scope.users = [];
  if (store.get("washerlist") != null) {
    list = store.get("washerlist");
    $scope.users = list;
  }

  var myPopup = null;

  var info = {};
  $scope.washer_type = 0;
  $rootScope.counter = 0;
  $scope.service_type = 0;
  $scope.requestSince = 0;
  $scope.clientRequest = {};
  $scope.washer_count = $stateParams.count;
  $scope.carddata = {};
  $scope.paypaldata = {};


  $scope.changeVehicle = function (val) {
    $scope.washer_type = val;
  }

  $scope.changeService = function (val) {
    $scope.service_type = val;
  }
  
  $scope.filterWasher = function () {
    if (list.length == 0) {
      var data = {};
      data.username = apiUsername;
      data.pwd = apiPassword;
      data.action = "getpaypalemail";
      data.user_id = store.get("userId");
      $.ajax({
        type: 'POST',
        url: apiUrl,
        data: data,
        contentType: "application/x-www-form-urlencoded",
        crossDomain: true
      }).then(function (data) {
        $scope.paypaldata = data;
      }, null);

      var data2 = {};
      data2.username = apiUsername;
      data2.pwd = apiPassword;
      data2.user_id = store.get("userId");
      data2.action = "getcarddata";
      $.ajax({
        type: 'POST',
        url: apiUrl,
        data: data2,
        contentType: "application/x-www-form-urlencoded",
        crossDomain: true
      }).then(function (data) {
        $scope.carddata = data;
      }, null);
    } else {
      for (var i = 0; i < list.length; i++) {
        if ($scope.service_type == 0 && list[i].is_elite_service == "1") {
          $scope.users.push(list[i]);
        } else if ($scope.service_type == 2 && list[i].is_standard_service == "1") {
          $scope.users.push(list[i]);
        } else if ($scope.service_type == 1 && list[i].is_premium_service == "1") {
          $scope.users.push(list[i]);
        } else {
          $scope.users.push(list[i]);
        }
      }
    }

  }
  $scope.getReqeuestStatus = function (id) {
    if ($rootScope.counter < 5*60) {
      var data = {};
      data.username = apiUsername;
      data.pwd = apiPassword;
      data.action = "washerdetails";
      data.client_request_id = id;
      $.ajax({
        type: 'POST',
        url: apiUrl,
        data: data,
        contentType: "application/x-www-form-urlencoded",
        crossDomain: true
      }).then(function (data) {

        $rootScope.counter = $rootScope.counter + 1;
        if (data.response.washer_id && data.code == 200) {
          $ionicLoading.hide();
          $rootScope.counter = 0;
          $interval.cancel($rootScope.requestStatus);
          $state.go("locate_washer", {
            "client_request_id": id, "userType": "washer", "washer_id": data.response.washer_id,
            "latitude": data.response.latitude, "longitude": data.response.longitude
          });
        }
        $scope.$digest();
      }, function (error) {
        $rootScope.counter = $rootScope.counter + 1;
        $ionicLoading.hide();
      });
    }
    else {
      $ionicLoading.hide();
      $rootScope.counter = 0;
      $interval.cancel($rootScope.requestStatus);
      var data = {};
      data.username = apiUsername;
      data.pwd = apiPassword;
      data.action = "cancelrequest";
      data.client_request_id = id;
      $.ajax({
        type: 'POST',
        url: apiUrl,
        data: data,
        contentType: "application/x-www-form-urlencoded",
        crossDomain: true
      }).then(function (data) {
        $ionicLoading.hide();
        if (data && data.code == 200) {
          $cordovaDialogs.alert("No Washer Accepted the request, so it is cancelled!", "Error", "Close");
          $state.go('home');
        }
        $scope.$digest();
      }, function (error) {
        $ionicLoading.hide();
      });

    }
  }
  $scope.timeAgo = function (objectDate) {
    var value = null;
    var todaysDate = new Date(new Date().toLocaleString('en-US', {
      timeZone: 'America/Phoenix'
    }));
    var msecPerMinute = 1000 * 60;
    var msecPerHour = msecPerMinute * 60;
    var msecPerDay = msecPerHour * 24;
    var msecPerWeek = msecPerDay * 7;
    var intervalTime = todaysDate.getTime() - new Date(objectDate).getTime();
    var week = Math.floor(intervalTime / msecPerWeek);
    intervalTime = intervalTime - (week * msecPerWeek);
    var days = Math.floor(intervalTime / msecPerDay);
    intervalTime = intervalTime - (days * msecPerDay);
    var hours = Math.floor(intervalTime / msecPerHour);
    intervalTime = intervalTime - (hours * msecPerHour);
    var minutes = Math.floor(intervalTime / msecPerMinute);
    intervalTime = intervalTime - (minutes * msecPerMinute);
    var seconds = Math.floor(intervalTime / 1000);
    if (week == 0 && hours == 0 && minutes < 5) {
      value = minutes;
    }
    return value;
  }
  $scope.cancelRequest = function (clientRequestDetail) {

    $interval.cancel($rootScope.requestStatus);

    $ionicLoading.show();
    var data = {};
    data.username = apiUsername;
    data.pwd = apiPassword;
    data.action = "cancelrequest";
    data.client_request_id = clientRequestDetail;
    $.ajax({
      type: 'POST',
      url: apiUrl,
      data: data,
      contentType: "application/x-www-form-urlencoded",
      crossDomain: true
    }).then(function (data) {
      $ionicLoading.hide();
      if (data && data.code == 200) {

        $state.go('home');
        $scope.$digest();
      }

    }, function (error) {
      $ionicLoading.hide();
    });
  }

  if ($scope.users.length == 0) {
    $scope.filterWasher();
  }

  $scope.validateClientRequest = function () {
    if (myPopup != null) {
      myPopup.close();
    }
    var price = store.get("amount");
    $scope.data = {}
    myPopup = $ionicPopup.show({
      template: price + '$',
      title: 'Price',
      scope: $scope,

      buttons: [
        {
          text: 'Cancel',
          onTap: function (e) {
            store.set("amount", 0);
            myPopup.close();
          }

        }, {
          text: '<b>Submit</b>',
          type: 'button-positive',
          onTap: function (e) {
            myPopup.close();
            $ionicLoading.show();
            if ($scope.users.length > 0) {
              PayPalMobile.clientMetadataID(function (result) {
                var data = {};
                data.username = apiUsername;
                data.pwd = apiPassword;
                data.action = "checkpaymentstatus";
                data.user_id = store.get("userId");
                $.ajax({
                  type: 'POST',
                  url: apiUrl,
                  data: data,
                  contentType: "application/x-www-form-urlencoded",
                  crossDomain: true
                }).then(function (data) {
                  if (data.response.data.status == 1) {
                    $ionicLoading.show();
                    var data = {};
                    data.username = apiUsername;
                    data.pwd = apiPassword;
                    data.action = "request";
                    data.vehicle_type = $scope.washer_type;
                    data.service_type = $scope.service_type;
                    data.user_id = store.get("userId");
                    data.users = $scope.users;
                    data.client_meta_data_id = result;
                    data.points = price;
                    $.ajax({
                      type: 'POST',
                      url: apiUrl,
                      data: data,
                      contentType: "application/x-www-form-urlencoded",
                      crossDomain: true
                    }).then(function (data) {
                      $ionicLoading.hide();
                      $ionicLoading.show({
                        template: 'Request Sent successfully',
                        duration: 2000
                      });
                      if (data && data.code == 200) {
                        $ionicLoading.show({
                          content: 'Loading',
                          animation: 'fade-in',
                          showBackdrop: true,
                          template: '<div><ion-spinner></ion-spinner><br>Waiting for washer to Accept Request from ' + $scope.users.length + ' washers</div><a class="button" ng-click="cancelRequest(' + data.success + ')" style="background: red;color: white;">Cancel</a>',
                          duration: 6000000,
                          scope: $scope
                        });
                        $rootScope.requestStatus = $interval(function () {
                          $scope.getReqeuestStatus(data.success);
                        }, 1000);
                      }
                    }, function (error) {
                      $ionicLoading.hide();
                    });
                  }
                  else {
                    $ionicLoading.hide();
                    $cordovaDialogs.alert("Please add a payment method!", "Add Payment Method", "Ok");
                    $state.go('add_points');
                  }
                }, function (error) {
                  $ionicLoading.hide();
                });
              });


            } else {
              if ($scope.carddata.response == undefined && $scope.paypaldata.response == undefined) {
                $ionicLoading.show({
                  template: 'No payment method',
                  duration: 2000
                }).then(function () {
                  $state.go("add_points");
                });
              }
              else {
                $ionicLoading.show({
                  template: 'Sorry no washer nearby, please try again later',
                  duration: 2000
                });
              }
            }
          }
        }
      ]
    });
  };

  $scope.getClientProfile = function (clientRequestForm, info) {
    var data = {};
    data.username = apiUsername;
    data.pwd = apiPassword;
    data.action = "getpriceofservice";
    data.vhicle_type = info.vehicle;
    data.service_type = info.service;
    $.ajax({
      type: 'POST',
      url: apiUrl,
      data: data,
      contentType: "application/x-www-form-urlencoded",
      crossDomain: true
    }).then(function (data) {
      store.set("amount", data.response.service_price);
      $scope.validateClientRequest(clientRequestForm);
    }, null);

    var data = {};
    data.username = apiUsername;
    data.pwd = apiPassword;
    data.action = "getprofile";
    data.user_id = store.get("userId");
    $.ajax({
      type: 'POST',
      url: apiUrl,
      data: data,
      contentType: "application/x-www-form-urlencoded",
      crossDomain: true
    }).then(function (data) {
      if (data && data.response) {
      }
    }, null);
  }
  $scope.showPopup = function () {
    $scope.data = {}

    // Custom popup
    myPopup = $ionicPopup.show({
      template: '<input type = "text" ng-model = "data.model">',
      title: 'Title',
      subTitle: 'Subtitle',
      scope: $scope,

      buttons: [
        { text: 'Cancel' }, {
          text: '<b>Save</b>',
          type: 'button-positive',
          onTap: function (e) {
            myPopup.close();

            if (!$scope.data.model) {
              e.preventDefault();
            } else {
              return $scope.data.model;
            }
          }
        }
      ]
    });
  };

});
